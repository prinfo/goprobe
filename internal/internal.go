package internal

import (
	"flag"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/prinfo/gocollect/config"
)

const usage string = `Usage: 
  gocollect [flags]

Flags:
 -c, --config		Configuration file (default "/etc/gocollect/gocollect.conf")
 -h, --help		Prints usage
 -v, --version		Prints GoCollect version
`

func init() {
	log.SetFormatter(&log.TextFormatter{
		ForceColors:            true,
		DisableLevelTruncation: true,
		FullTimestamp:          true,
		DisableTimestamp:       false,
		TimestampFormat:        "2006-01-02 15:04:05",
	})

	var configFile string
	var version bool
	flag.StringVar(&configFile, "c", "/etc/gocollect/gocollect.conf", "")
	flag.StringVar(&configFile, "config", "/etc/gocollect/gocollect.conf", "")
	flag.BoolVar(&version, "v", false, "")
	flag.BoolVar(&version, "version", false, "")
	flag.Usage = func() { fmt.Print(usage) }
	flag.Parse()

	if version {
		fmt.Println("GoCollect", Version)
		os.Exit(0)
	}

	if err := config.Load(configFile); err != nil {
		log.Error(err)
		os.Exit(1)
	}
}
