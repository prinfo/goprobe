# GoCollect

GoCollect collects and writes metrics and stats with various plugins.

Input Plugins 
-------

* Packetfront's BECS
* MPEG Transport Stream

Output Plugins 
-------

* InfluxDB
* Stdout
* UDP
* HTTP