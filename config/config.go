package config

import (
	"github.com/pelletier/go-toml"
)

var ConfigTree *toml.Tree

func Load(filename string) error {
	var err error
	ConfigTree, err = toml.LoadFile(filename)
	return err
}
