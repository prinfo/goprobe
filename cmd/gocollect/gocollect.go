package main

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	_ "gitlab.com/prinfo/gocollect/internal"
	_ "gitlab.com/prinfo/gocollect/plugins"
	"gitlab.com/prinfo/gocollect/plugins/inputs"
	"gitlab.com/prinfo/gocollect/plugins/outputs"
)

func main() {
	outputs := outputs.Plugins()
	for name, plugin := range *outputs {
		log.Infof("Loading %s output plugin...\n", name)
		plugin.Start()
		defer plugin.Stop()
	}

	inputs := inputs.Plugins()
	for name, plugin := range *inputs {
		log.Infof("Loading %s input plugin...\n", name)
		go plugin.Start()
		defer plugin.Stop()
	}

	sigs := make(chan os.Signal, 1)
	waitForExit := make(chan bool, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		waitForExit <- true
	}()

	log.Info("GoCollect started")
	<-waitForExit
	log.Info("GoCollect stopped")
}
