package aggregator

import (
	"gitlab.com/prinfo/gocollect/plugins/outputs"
)

func Add(title string, tags map[string]string, fields map[string]interface{}) {
	outputs := outputs.Plugins()
	for _, plugin := range *outputs {
		go plugin.Push(title, tags, fields)
	}
}
