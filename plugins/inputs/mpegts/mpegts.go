package mpegts

import (
	"log"
	"net"
	"strconv"
	"strings"
	"time"

	"gitlab.com/prinfo/gocollect/aggregator"
	"gitlab.com/prinfo/gocollect/plugins/inputs"
)

const (
	packetSize = 188
	patPid     = 0x00
	pcrFlag    = 0x10
)

type packet []byte
type adaptationField []byte

type MPEGTS struct {
	Interface     string              `toml:"interface"`
	CC            bool                `toml:"cc"`
	PAT           bool                `toml:"pat"`
	PMT           bool                `toml:"pmt"`
	PCR           bool                `toml:"pcr"`
	PAT_Threshold time.Duration       `toml:"pat_threshold"`
	PMT_Threshold time.Duration       `toml:"pmt_threshold"`
	PCR_Threshold int64               `toml:"pcr_threshold"`
	Channels      []map[string]string `toml:"channels"`
}

func init() {
	name := "mpeg-ts"
	log.SetPrefix(name + ": ")
	inputs.Add(name, &MPEGTS{})
}

func (m *MPEGTS) Start() {
	iface, err := net.InterfaceByName(m.Interface)
	if err != nil {
		log.Println(err)
		return
	}

	for _, channelMap := range m.Channels {
		for channelName, group := range channelMap {
			go m.collect(channelName, group, iface)
		}
	}
}

func (m *MPEGTS) Stop() {

}

func (m *MPEGTS) collect(channelName, group string, iface *net.Interface) {
	gp := strings.Split(group, ":")
	if len(gp) != 2 {
		log.Printf(`channel "%s" is incorrect. Correct syntax is "<channel name>": "<address:port>"`, channelName)
		return
	}

	ip := net.ParseIP(gp[0])
	port, err := strconv.Atoi(gp[1])
	if !ip.IsMulticast() || err != nil || port <= 1024 {
		log.Printf("invalid multicast address or port: %s %s", channelName, group)
		return
	}

	conn, err := net.ListenMulticastUDP("udp4", iface, &net.UDPAddr{IP: ip, Port: port})
	if err != nil {
		log.Printf("could not join %s (%s): %s", channelName, group, err.Error())
		return
	}
	defer conn.Close()

	pastCCSet := make(map[uint16]byte)
	ccSet := make(map[uint16]byte)
	pcrSet := make(map[uint16]uint64)
	pmtSet := make(map[uint16]*time.Ticker)

	patTimer := time.NewTicker(m.PAT_Threshold * time.Millisecond)
	go func() {
		for {
			<-patTimer.C
			if m.PAT {
				go aggregator.Add("mpegts_pat", map[string]string{"channel": channelName}, map[string]interface{}{"errors": 1})
			}
		}
	}()

	data := make([]byte, 1316)
	for {
		size, _, err := conn.ReadFromUDP(data)
		if err != nil {
			log.Println("ReadFromUDP failed:", err)
			continue
		}

		// Read transport streams
		for i := 0; i < size; i += packetSize {
			pkt := packet(data[i : packetSize+i])

			pid := pkt.pid()
			if pid == patPid {
				patTimer.Reset(m.PAT_Threshold * time.Millisecond)

				if m.PMT {
					var pos uint16 = 0
					if pkt.hasAdaptationField() {
						pos = uint16(pkt.adaptationFieldLength() + 1) // length itself is 1 byte
					}

					// Loop through all programs within the PAT
					length := pkt.patLength(pos)
					for p := pos + 13; p <= pos+length; p += 4 {
						program := pkt.programID(p)
						if _, exists := pmtSet[program]; exists {
							pmtSet[program].Reset(m.PMT_Threshold * time.Millisecond)

						} else {
							pmtTimer := time.NewTicker(m.PMT_Threshold * time.Millisecond)
							go func() {
								for {
									<-pmtTimer.C
									go aggregator.Add("mpegts_pmt", map[string]string{"channel": channelName}, map[string]interface{}{"errors": 1})
								}
							}()
							pmtSet[program] = pmtTimer
						}
					}
				}

			} else if pid < 32 || pid > 8186 {
				continue
			}

			cc := pkt.cc()
			ccErr := false
			if _, ok := ccSet[pid]; ok {
				if cc == pastCCSet[pid] {
					ccErr = true
				} else if cc == ccSet[pid] {
				} else if cc == 0 && ccSet[pid] != 15 {
					ccErr = true
				} else if cc != 0 && cc-1 != ccSet[pid] {
					ccErr = true
				}

				if m.CC && ccErr {
					go aggregator.Add("mpegts_continuity", map[string]string{"channel": channelName}, map[string]interface{}{"errors": 1})
				}
			}
			pastCCSet[pid] = ccSet[pid]
			ccSet[pid] = cc

			if pkt.hasAdaptationField() {
				af := pkt.adaptationField()
				if len(af) > 0 && af.hasPCRFlag() {
					pcr := af.pcr()
					if _, ok := pcrSet[pid]; ok {
						prevSeconds := float64(pcrSet[pid]) / 27000000
						seconds := float64(pcr) / 27000000
						pcrDiff := (seconds - prevSeconds) * 1000
						if pcrDiff > float64(m.PCR_Threshold) && m.PCR {
							go aggregator.Add("mpegts_pcr", map[string]string{"channel": channelName}, map[string]interface{}{"errors": 1})
						}
					}
					pcrSet[pid] = pcr
				}
			}
		}
	}
}

func (p packet) pid() uint16 {
	return uint16(p[1]&0x1f)<<8 | uint16(p[2])
}

func (p packet) cc() byte {
	return p[3] & uint8(0x0f)
}

func (p packet) hasAdaptationField() bool {
	return p[3]&0x20 == 0x20 || p[3]&0x20 == 0x30
}

func (p packet) adaptationFieldLength() byte {
	return p[4]
}

func (p packet) adaptationField() adaptationField {
	return adaptationField(p[5 : p.adaptationFieldLength()+5])
}

func (p packet) patLength(index uint16) uint16 {
	return uint16(p[index+6]&0x0f)<<8 | uint16(p[index+7])
}

func (p packet) programID(index uint16) uint16 {
	return uint16(p[index])<<8 | uint16(p[index+1])
}

func (af adaptationField) hasPCRFlag() bool {
	return af[0]&0x10 == pcrFlag
}

func (af adaptationField) pcr() uint64 {
	bytes := af[1:7]
	var a, b, c, d, e, f uint64
	a = uint64(bytes[0])
	b = uint64(bytes[1])
	c = uint64(bytes[2])
	d = uint64(bytes[3])
	e = uint64(bytes[4])
	f = uint64(bytes[5])
	base := (a << 25) | (b << 17) | (c << 9) | (d << 1) | (e >> 7)
	ext := ((e & 0x1) << 8) | f
	return base*300 + ext
}
