package inputs

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/pelletier/go-toml"
	"gitlab.com/prinfo/gocollect/config"
)

type Plugin interface {
	Start()
	Stop()
}

var plugins = make(map[string]Plugin)

func Add(name string, plugin Plugin) {
	t, ok := config.ConfigTree.Get("input." + strings.ToLower(name)).(*toml.Tree)
	if !ok {
		return
	}

	if err := t.Unmarshal(plugin); err != nil {
		log.Error("error parsing input", name)
		os.Exit(1)
	}

	plugins[strings.ToLower(name)] = plugin
}

func Plugins() *map[string]Plugin {
	return &plugins
}
