package becs

import (
	"log"
	"regexp"
	"time"

	"gitlab.com/prinfo/becsclient"
	"gitlab.com/prinfo/gocollect/aggregator"
	"gitlab.com/prinfo/gocollect/plugins/inputs"
)

type Becs struct {
	URL      string        `toml:"url"`
	Username string        `toml:"username"`
	Password string        `toml:"password"`
	Interval time.Duration `toml:"interval"`
	timer    *time.Ticker
}

var host string

func init() {
	name := "becs"
	log.SetPrefix(name + ": ")
	inputs.Add(name, &Becs{})
}

func (b *Becs) Start() {
	reg, _ := regexp.Compile(`^https?:\/\/|:\d+$`)
	host = reg.ReplaceAllString(b.URL, "")

	b.timer = time.NewTicker(b.Interval * time.Second)
	go func() {
		for {
			<-b.timer.C
			b.collect()
		}
	}()
}

func (b *Becs) Stop() {
	b.timer.Stop()
}

func (b *Becs) collect() {
	if err := b.checkSession(); err != nil {
		log.Println(err)
		return
	}

	apps, err := becsclient.ApplicationList()
	if err != nil {
		return
	}

	for _, appName := range apps {
		app, err := becsclient.ApplicationStatusGet(appName, false)
		if err != nil {
			continue
		}

		tags := make(map[string]string)
		fields := make(map[string]interface{})

		tags["application"] = appName
		tags["host"] = app.Hostname
		tags["version"] = app.Version
		fields["uptime"] = int(app.UpTime)
		fields["cpu_usage"] = int(app.CPUUsage)
		fields["cpu_average_60"] = int(app.CPUAverage60)
		go aggregator.Add("becs_modules", tags, fields)
	}

	metrics, err := becsclient.MetricGet("em_elements")
	if err != nil {
		return
	}

	for _, item := range metrics[0].Values.Items {
		tags := make(map[string]string)
		fields := make(map[string]interface{})
		tags["host"] = host
		tags["metric"] = metrics[0].Name
		fields["value"] = int(item.Value)

		for _, label := range item.Labels.Items {
			tags[label.Name] = label.Value
		}
		go aggregator.Add("becs_metrics", tags, fields)
	}
}

// CheckSession pings BECS to update the session. If the ping fails it tries to login
func (b *Becs) checkSession() error {
	if err := becsclient.SessionPing(); err != nil {
		if err := becsclient.SessionLogin(b.URL, b.Username, b.Password); err != nil {
			return err
		}
	}
	return nil
}
