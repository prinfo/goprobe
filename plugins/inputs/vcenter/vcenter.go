package vcenter

import (
	"log"

	"gitlab.com/prinfo/gocollect/plugins/inputs"
)

type VCenter struct {
}

func init() {
	name := "vcenter"
	log.SetPrefix(name + ": ")
	inputs.Add(name, &VCenter{})
}

func (v *VCenter) Start() {

}

func (v *VCenter) Stop() {
}
