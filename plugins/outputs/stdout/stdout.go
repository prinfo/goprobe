package stdout

import (
	"encoding/json"
	"fmt"

	"gitlab.com/prinfo/gocollect/plugins/outputs"
)

type StdOut struct {
}

func init() {
	outputs.Add("stdout", &StdOut{})
}

func (s *StdOut) Start() {
}

func (s *StdOut) Stop() {
}

func (s *StdOut) Push(title string, tags map[string]string, fields map[string]interface{}) {
	toJson := struct {
		Title  string                 `json:"title,omitempty"`
		Tags   map[string]string      `json:"tags,omitempty"`
		Fields map[string]interface{} `json:"fields,omitempty"`
	}{
		Title:  title,
		Tags:   tags,
		Fields: fields,
	}

	data, _ := json.Marshal(&toJson)
	fmt.Println(string(data))
}
