package influxdb

import (
	"os"
	"time"

	influx "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"gitlab.com/prinfo/gocollect/plugins/outputs"
)

type InfluxDB struct {
	URL           string        `toml:"url"`
	Database      string        `toml:"database"`
	RP            string        `toml:"retention_policy"`
	BatchSize     uint          `toml:"batch_size"`
	FlushInterval time.Duration `toml:"flush_interval"`
	LogLevel      uint          `toml:"log_level"`
	api           api.WriteAPI
	client        influx.Client
	hostname      string
}

func init() {
	outputs.Add("influxdb", &InfluxDB{})
}

func (i *InfluxDB) Start() {
	i.hostname, _ = os.Hostname()
	options := influx.Options{}
	options.SetMaxRetries(3)
	options.SetHTTPRequestTimeout(10)
	options.SetLogLevel(i.LogLevel)
	options.SetBatchSize(i.BatchSize)
	options.SetFlushInterval(uint(i.FlushInterval))

	i.client = influx.NewClientWithOptions(i.URL, "", &options)
	i.api = i.client.WriteAPI("", i.Database+"/"+i.RP)
}

func (i *InfluxDB) Stop() {
	i.api.Flush()
}

func (i *InfluxDB) Push(measurement string, tags map[string]string, fields map[string]interface{}) {
	p := influx.NewPoint(
		measurement,
		tags,
		fields,
		time.Now(),
	)
	p.AddTag("server", i.hostname)
	i.api.WritePoint(p)
}
