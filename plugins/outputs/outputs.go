package outputs

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/pelletier/go-toml"
	"gitlab.com/prinfo/gocollect/config"
)

type Plugin interface {
	Start()
	Stop()
	Push(string, map[string]string, map[string]interface{})
}

var plugins = make(map[string]Plugin)

func Add(name string, plugin Plugin) {
	t, ok := config.ConfigTree.Get("output." + strings.ToLower(name)).(*toml.Tree)
	if !ok {
		return
	}

	if err := t.Unmarshal(plugin); err != nil {
		log.Error("error parsing output", name)
		os.Exit(1)
	}

	plugins[strings.ToLower(name)] = plugin
}

func Plugins() *map[string]Plugin {
	return &plugins
}
