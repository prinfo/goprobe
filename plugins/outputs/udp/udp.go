package udp

import (
	"encoding/json"
	"log"
	"net"

	"gitlab.com/prinfo/gocollect/plugins/outputs"
)

type UDP struct {
	Server string `toml:"server"`
	Port   int    `toml:"port"`
	*net.UDPConn
}

func init() {
	name := "udp"
	log.SetPrefix(name + ": ")
	outputs.Add(name, &UDP{})
}

func (u *UDP) Start() {
	ip := net.ParseIP(u.Server)
	rAddr := &net.UDPAddr{
		IP:   ip,
		Port: u.Port,
	}

	var err error
	u.UDPConn, err = net.DialUDP("udp4", nil, rAddr)
	if err != nil {
		log.Println(err)
	}
}

func (u *UDP) Stop() {
	u.Close()
}

func (u *UDP) Push(title string, tags map[string]string, fields map[string]interface{}) {
	toJson := struct {
		Title  string                 `json:"title,omitempty"`
		Tags   map[string]string      `json:"tags,omitempty"`
		Fields map[string]interface{} `json:"fields,omitempty"`
	}{
		Title:  title,
		Tags:   tags,
		Fields: fields,
	}

	data, _ := json.Marshal(&toJson)
	_, err := u.Write(data)
	if err != nil {
		log.Println(err)
	}
}
