package http

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/prinfo/gocollect/plugins/outputs"
)

type HTTP struct {
	URL    string `toml:"url"`
	client http.Client
}

func init() {
	outputs.Add("http", &HTTP{})
}

func (h *HTTP) Start() {
	h.client.Timeout = time.Second * 5
}

func (h *HTTP) Stop() {
}

func (h *HTTP) Push(title string, tags map[string]string, fields map[string]interface{}) {
	toJson := struct {
		Title  string                 `json:"title,omitempty"`
		Tags   map[string]string      `json:"tags,omitempty"`
		Fields map[string]interface{} `json:"fields,omitempty"`
	}{
		Title:  title,
		Tags:   tags,
		Fields: fields,
	}

	data, _ := json.Marshal(&toJson)
	resp, err := h.client.Post(h.URL, "application/json", bytes.NewReader(data))
	if err != nil {
		log.Println(err)
		return
	}

	resp.Body.Close()
}
