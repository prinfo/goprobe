**Plugins**

An input plugin needs to implement the inputs.Plugin interface.

An output plugin needs to implement the outputs.Plugin interface.

All plugins need a init() function that calls inputs/output.Add(), they also need to be added as a blank import in the plugins.go file.
