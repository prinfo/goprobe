package plugins

// Blank imports for plugin inits
import (
	_ "gitlab.com/prinfo/gocollect/plugins/inputs/becs"
	_ "gitlab.com/prinfo/gocollect/plugins/inputs/mpegts"
	_ "gitlab.com/prinfo/gocollect/plugins/inputs/vcenter"
	_ "gitlab.com/prinfo/gocollect/plugins/outputs/http"
	_ "gitlab.com/prinfo/gocollect/plugins/outputs/influxdb"
	_ "gitlab.com/prinfo/gocollect/plugins/outputs/stdout"
	_ "gitlab.com/prinfo/gocollect/plugins/outputs/udp"
)
