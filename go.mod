module gitlab.com/prinfo/gocollect

go 1.23

require (
	github.com/influxdata/influxdb-client-go/v2 v2.3.0
	github.com/pelletier/go-toml v1.9.1
	gitlab.com/prinfo/becsclient v1.0.0
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)

require (
	github.com/deepmap/oapi-codegen v1.6.0 // indirect
	github.com/influxdata/line-protocol v0.0.0-20200327222509-2487e7298839 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/net v0.23.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
